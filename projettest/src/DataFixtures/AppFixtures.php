<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Eleve;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 10; $i++){
            $eleve = new Eleve();
            $eleve->setNom("Nom $i")
                  ->setprenom("Prenom $i")
                  ->setdatenaiss(new \DateTime())
                  ->setmoyenne($i);

                  $manager->persist($eleve);
        }

        $manager->flush();
    }
}
