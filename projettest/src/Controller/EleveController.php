<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Eleve;

class EleveController extends AbstractController
{
    /**
     * @Route("/eleve", name="eleve")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Eleve::class);

        $eleves = $repo->findAll();

        return $this->render('eleve/index.html.twig', [
            'controller_name' => 'EleveController',
            'eleves' => $eleves,
        ]);

    }
    /**
     * @Route("/", name="home")
     */
    public function home() {
            return $this->render('eleve/home.html.twig', [
                'title' => "Bienvenue"
                ]);
    }
    /**
     * @Route("/eleve/ajouter", name="ajouter")
     */
    public function ajouter(Request $request, EntityManagerInterface  $manager){
        $eleve = new Eleve();
        $form = $this->createFormBuilder($eleve)
                     ->add('nom')
                     ->add('prenom')
                     ->add('datenaiss')
                     ->add('moyenne')
                     ->add('save',submitType::class)
                     ->getForm();   
                     
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($eleve);
            $manager->flush();

            return $this->redirectToRoute('liste');
        }

        return $this->render('eleve/ajouter.html.twig',[
            'formEleve' => $form->createView()
        ]);
    }
    /**
     * @Route("/eleve/liste", name="liste")
     */
    public function liste(){

        $repo = $this->getDoctrine()->getRepository(Eleve::class);

        $eleves = $repo->findAll();

        return $this->render('eleve/liste.html.twig',[
            'controller_name' => 'EleveController',
            'eleves' => $eleves,
        ]
    );
    }
}
